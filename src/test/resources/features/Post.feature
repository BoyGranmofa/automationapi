@smoke @APITest @CreateUser
Feature: Create a new user

  @Post
  Scenario: Normal Create a new user
    Given I perform GET operation for list body "Yhosuaa Sirait12" "Yhosuaa5908@gmail.com" "male" "active"
    Then I should receive a 201 status code

  @Invalid_Token
  Scenario: Access Token Not Valid
    Given I provide an invalid access token for list body "Yosua Philip123" "Yhosua@gmail.com" "male" "active"
    Then  I should receive a 401 status code and get message "Invalid token"

  @Invalid_Email
  Scenario: Email Not Valid and use
    Given I provide an invalid email for list body "Yosua Philip123" "Yhosuagmail.com" "male" "active"
    Then  I should receive a 422 status code

#  @Invalid_Authentication
#  Scenario: Authentication not valid
#    Given I provide Incomplete authentication for list body "Yhosuagmail.com"
#    Then  I should receive a 401 status code and get message "Authentication failed"
