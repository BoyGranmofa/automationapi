@smoke @APITest @GetUser

Feature: Get User

  @Get
  Scenario: Normal Valid Get User from ID
    Given I perform GET operation for 6927436
    Then I should see the author name as "Boy Sihombing" and status code 200

  @InvalidGet
  Scenario: Invalid Get User from ID
    Given I perform GET operation for 6927473
    Then I should see status code 404



