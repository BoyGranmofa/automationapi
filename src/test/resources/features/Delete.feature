@smoke @APITest @DeleteUser
Feature: Delete user

  @Delete
  Scenario: Normal I want to Delete user
    Given I delete the user with id 6929594
    When I should see the status code 204
    Then I verify the user is deleted with status code 404

  @Invalid_Delete
  Scenario: User Not Found
    Given I delete the user with id 6927530
    When I should see the status code 404