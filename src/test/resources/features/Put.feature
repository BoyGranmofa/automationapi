@smoke @APITest @UpdateUser
Feature: Update a new user

  @Put
  Scenario: Normal I want Update user details
    Given I update the user with id 6927589 to have name "Cinta Sihombing", email "Cinta123@gmail.com", gender "female", status "active"
    Then I should see the author name as "Cinta Sihombing"

  @Invalid_Entity
  Scenario: Entity Not Found
    Given I attempt to update the entity id 6927490 to have name "Boy", email "Boy@gmail.com", gender "female", status "active"
    Then the response status code should be 404