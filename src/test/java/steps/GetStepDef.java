package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;

public class GetStepDef {
    public static Response response;

    @Given("I perform GET operation for {int}")
    public void iPerformGETOperationFor(int userId) {
        String token = "9af9e5a6a4a7f03553c65b4c01f25d6da8d7729d857cc1ba11b401c8ab296a76";
        String baseUrl = "https://gorest.co.in/public/v2/users/" + userId;
        response = RestAssured
                .given()
                .header("Authorization", "Bearer " + token)
                .get(baseUrl);
    }

    @Then("I should see the author name as {string} and status code {int}")
    public void iShouldSeeTheAuthorNameAsAndStatusCode(String nameExpected, int statusCodeExpected) {
        int statusCodeActual = response.getStatusCode();
        String nameActual = response.jsonPath().getString("name");
//        System.out.println("actual name: " + nameActual);
        Assert.assertEquals(nameActual, nameExpected); // cari buat manggil name di hasil response nya
        Assert.assertEquals(statusCodeActual, statusCodeExpected);

    }

    @Then("I should see status code {int}")
    public void iShouldSeeStatusCode(int statusCodeExpected) {
        int actualStatusCode = response.getStatusCode();
//        System.out.println("status code: " + actualStatusCode);
        Assert.assertEquals(statusCodeExpected, actualStatusCode);
    }
}

//    @Then("I should see status code {string}")
//    public void iShouldSeeStatusCode(String statusCodeExpected) {
//        int statusCodeActual = response.statusCode();
//        String statusCodeActualAsString = String.valueOf(statusCodeActual);
////        System.out.println("status code: " + statusCodeActualAsString);
//        Assert.assertEquals(statusCodeActualAsString , statusCodeExpected);
//    }