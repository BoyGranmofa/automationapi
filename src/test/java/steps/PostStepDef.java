package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.Assert;

public class PostStepDef {
    public static Response response;
    public static String token = "9af9e5a6a4a7f03553c65b4c01f25d6da8d7729d857cc1ba11b401c8ab296a76";
    public static String baseUrl = "https://gorest.co.in/public/v2/users";

    @Given("I perform GET operation for list body {string} {string} {string} {string}")
    public void iPerformGETOperationForListBody(String name, String email, String gender, String status) {
        JSONObject request = new JSONObject();
        request.put("name", name);
        request.put("email", email);
        request.put("gender", gender);
        request.put("status", status);
//        System.out.println(request);
//        System.out.println(request.toJSONString());
        response = RestAssured
                .given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request.toJSONString())
                .post(baseUrl);
        System.out.println("test id : " + response.jsonPath().getString("id")); // untuk melihat id yg terbentuk
    }

    @Then("I should receive a {int} status code")
    public void iShouldReceiveAStatusCode(int statusCodeExpected) {
        int statusCodeActual = response.getStatusCode();
        Assert.assertEquals(statusCodeActual, statusCodeExpected);
    }

    @Given("I provide an invalid access token for list body {string} {string} {string} {string}")
    public void iProvideAnInvalidAccessTokenForListBody(String name, String email, String gender, String status) {
        JSONObject request = new JSONObject();
        request.put("name", name);
        request.put("email", email);
        request.put("gender", gender);
        request.put("status", status);

        // Prepare request with an invalid access token
        String token = "af9e5a6a4a7f03553c65b4c01f25d6da8d7729d857cc1ba11b401c8ab296a76";
        String baseUrl = "https://gorest.co.in/public/v2/users";
        response = RestAssured
                .given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request.toJSONString())
                .post(baseUrl);
    }

    @Given("I provide an invalid email for list body {string} {string} {string} {string}")
    public void iProvideAnInvalidEmailForListBody(String name, String email, String gender, String status) {
        JSONObject request = new JSONObject();
        request.put("name", name);
        request.put("email", email);
        request.put("gender", gender);
        request.put("status", status);
        response = RestAssured
                .given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request.toJSONString())
                .post(baseUrl);
    }

    @Then("I should receive a {int} status code and get message {string}")
    public void iShouldReceiveAStatusCodeAndGetMessage(int statusCodeExpected, String messageExpected) {
        int statusCodeActual = response.getStatusCode();
        String messageActual = response.jsonPath().getString("message");
        Assert.assertEquals(statusCodeActual, statusCodeExpected);
        Assert.assertEquals(messageActual, messageExpected);
    }

}

