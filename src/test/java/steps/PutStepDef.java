package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.Assert;

public class PutStepDef {
    public static Response response;
    public static String token = "9af9e5a6a4a7f03553c65b4c01f25d6da8d7729d857cc1ba11b401c8ab296a76";
    public static String baseUrl = "https://gorest.co.in/public/v2/users/";

    @Given("I update the user with id {int} to have name {string}, email {string}, gender {string}, status {string}")
    public void iUpdateTheUserWithIdToHaveNameEmailGenderStatus(int userID, String name, String email, String gender, String status) {
        JSONObject request = new JSONObject();
        request.put("name", name);
        request.put("email", email);
        request.put("gender", gender);
        request.put("status", status);

        response = RestAssured
                .given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request.toJSONString())
                .put(baseUrl + userID);
        System.out.println("Update Response: " + response.asString());
    }

    @Then("I should see the author name as {string}")
    public void iShouldSeeTheAuthorNameAs(String nameExpected) {
        String nameActual = response.jsonPath().getString("name");
        Assert.assertEquals(nameActual, nameExpected);
    }

    @Given("I attempt to update the entity id {int} to have name {string}, email {string}, gender {string}, status {string}")
    public void iAttemptToUpdateTheEntityIdToHaveNameEmailGenderStatus(int userID, String name, String email, String gender, String status) {
        JSONObject request = new JSONObject();
        request.put("name", name);
        request.put("email", email);
        request.put("gender", gender);
        request.put("status", status);

        response = RestAssured
                .given()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request.toJSONString())
                .put(baseUrl + userID);
        System.out.println("Update Response: " + response.asString());
    }

    @Then("the response status code should be {int}")
    public void theResponseStatusCodeShouldBe(int statusCodeExpected) {
        int statusCodeActual = response.getStatusCode();
        Assert.assertEquals(statusCodeActual, statusCodeExpected);
    }
}
