package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;

public class DeleteStepDef {
    public static Response response;
    @Given("I delete the user with id {int}")
    public void iDeleteTheUserWithId(int userId) {
        String token = "9af9e5a6a4a7f03553c65b4c01f25d6da8d7729d857cc1ba11b401c8ab296a76";
        String baseUrl = "https://gorest.co.in/public/v2/users/" + userId;
        response = RestAssured
                .given()
                .header("Authorization", "Bearer " + token)
                .delete(baseUrl);
    }

    @When("I should see the status code {int}")
    public void iShouldSeeTheStatusCode(int statusCodeExpected) {
        int statusCodeActual = response.getStatusCode();
//        System.out.println("status code: " + statusCodeActualAsString);
        Assert.assertEquals(statusCodeActual , statusCodeExpected);
    }

    @Then("I verify the user is deleted with status code {int}")
    public void iVerifyTheUserIsDeletedWithStatusCode(int statusCodeExpected) {
        String token = "9af9e5a6a4a7f03553c65b4c01f25d6da8d7729d857cc1ba11b401c8ab296a76"; // Ganti dengan token akses Anda
        String baseUrl = "https://gorest.co.in/public/v2/users" + statusCodeExpected;
        response = RestAssured
                .given()
                .header("Authorization", "Bearer " + token)
                .get(baseUrl);
    }
}
