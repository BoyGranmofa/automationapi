package runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/apiTesting/cucumber-report.json",  "html:target/results/apiTesting/index.html"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = "@APITest"

)
public class GetRunner  extends AbstractTestNGCucumberTests {
}
