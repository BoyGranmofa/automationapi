# AutomationAPI

Pengujian otomatisasi Application Programming Interface (API) adalah jenis pengujian otomatis yang berfokus pada kinerja dan fungsionalitas API.

API (Application Programming Interface) adalah suatu kumpulan aturan dan protokol yang memungkinkan dua buah perangkat lunak atau aplikasi berkomunikasi dan berinteraksi satu sama lain.


## Getting started

Halo perkenalkan nama saya Boy Sihombing, biasa dipanggil Boy

Pada test API Automation ini saya membuat project menggunakan metedo Gherkin Maven menggunakan bahasa java,
didalam project akan ada file di scr > test. Di dalam file test terdapat dua folder yaitu java dan resources.
1. File Java
    - Runner, yang bertujuan untuk menjalankan scenario berdasarkan apa yang ada dalam tag
    - Steps, yang bertujuan untuk menjalankan bahasa mesin atau berjalan di belakang layar
2. Resources
    - features, fitur yang menjelaskan jalannya scenario secara bahasa manusia

## Description
 Dalam pengujian ini terdapat 4 fitur yang akan di test yaitu Get, Post, Put, Delete

## Test
Saya akan menjelaskan secara singkat salah satu scenario yaitu get

```Gherkin
@Get
Scenario: Normal Valid Get User from ID // nama untuk scenario
Given I perform GET operation for 6927436 // saya akan melakukan operasi Get dengan id berikut
Then I should see the author name as "Boy Sihombing" and status code 200 // ini adalah hasil dari proses jika berhasil maka akan menverifikasi hasil dari id dengan "nama" dan "Code"
```
didalam runner akan menjalankan berdasarkan tag, bisa secara perscenario dan semua berdasarkan tag, contoh :
```Gherkin
tags = "@Get"
```
untuk hasil report bisa dilihat pada
```Gherkin
plugin = {"json:target/results/apiTesting/cucumber-report.json",
```

Scenario Get
- Normal Valid Get User from ID
- Invalid Get User from ID

Scenario Post
- Normal Create a new user
- Access Token Not Valid
- Email Not Valid and use

Scenario Put
- Normal I want Update user details
- Entity Not Found

Scenario Delete
- Normal I want to Delete user
- User Not Found


Berikut video project yang saya buat,
https://drive.google.com/file/d/1qaozo0ibXReAm2yGRckGade9xl7nzn2M/view?usp=sharing

Terimakasih...